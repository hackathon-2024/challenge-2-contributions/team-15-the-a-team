#!/bin/sh

# SPDX-FileCopyrightText: 2024 the-a-team
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

python app_vllm.py --model ${VLLM_MODEL} --tensor-parallel-size 1 --gpu-memory-utilization 0.4 --host localhost --port 8000
