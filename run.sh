#!/usr/bin/bash

# SPDX-FileCopyrightText: 2024 the-a-team
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

echo "Salut les Musclés !"

set -e

export SCARF_NO_ANALYTICS=true
export TRANSFORMERS_OFFLINE=1
export HF_DATASETS_OFFLINE=1
export ANONYMIZED_TELEMETRY=False

QUESTIONS=$(realpath $1)
OUTPUT=$(realpath $2)

ROOT=$(dirname -- $0)
cd $ROOT

module purge
#module load cpuarch/amd
module load pytorch-gpu/py3/2.1.1
#source .venv/bin/activate

pip install --no-cache-dir -r requirements_2.txt

python app_vllm.py --model /gpfsdswork/dataset/HuggingFace_Models/meta-llama/Llama-2-13b-chat-hf --tensor-parallel-size 1 --gpu-memory-utilization 0.4 --host localhost --port 8000 &

python rag.py "$QUESTIONS" "$OUTPUT"

